# Changes in srhinow/srlayer

## 3.1.7 (09.06.2022)
- fix dca tl_modules.php: remove 'rte' => false, 'mandatory' => false

## 3.1.6 (07.06.2022)
- #8: add/remove class 'active' to layer

## 3.1.5 (07.06.2022)
- #7: add tl_class clr to field 'srl_set_duration'
- #9: remove eval/rte from field 'srl_content'

## 3.1.4 (14.07.2021)
- add opnClass-Definition in modul-settings and rewrite for multiple layer on one page
